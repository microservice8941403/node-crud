const express = require("express");
const bodyParser = require("body-parser");
const product = require("./api/product");

const app = express();
const port = 5000;
app.use(bodyParser.json());

app.use("/api", product);

// Start the server
app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
