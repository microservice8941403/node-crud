const winston = require('winston');

// Buat logger
const logger = winston.createLogger({
  transports: [
    // Transport untuk menulis ke file
    new winston.transports.File({ filename: 'app.log' }),
    // Transport untuk menampilkan pesan di konsol
    new winston.transports.Console()
  ],
  format: winston.format.combine(
    winston.format.timestamp(),
    winston.format.json()
  )
});

// Ekspor logger agar bisa digunakan di modul lain
module.exports = logger;
