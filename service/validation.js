const Joi = require("joi");

// Validation function
const validateProductPost = (body) => {
  const schema = Joi.object({
    name: Joi.string().required(),
    description: Joi.string().optional(),
    price: Joi.number().optional(),
    stock: Joi.number().optional(),
  });
  return schema.validate(body);
};

const validateProductUpdate = (body) => {
  const schema = Joi.object({
    id: Joi.number().required(),
  });
  return schema.validate(body);
};

module.exports = {
  validateProductPost,
  validateProductUpdate,
};
