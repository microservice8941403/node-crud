const Logger = require("../service/logger");
const DBservice = require("../service/database");

const getProduct = (req) => {
  return new Promise(async (resolve, reject) => {
    const { id } = req.query;
    let xquery;
    try {
      if (id) {
        xquery = `SELECT * FROM product WHERE id = ${id}`;
      } else {
        xquery = `SELECT * FROM product`;
      }

      const Response = await DBservice.runQuery(xquery);

      Logger.info(["Product Controller", "SUCCESS Get Product"]);
      resolve(Response);
    } catch (err) {
      Logger.info(["Product Controller", "ERROR Get Product"], err.message);
      reject({ "err.message": err });
    }
  });
};

const postProduct = (req) => {
  return new Promise(async (resolve, reject) => {
    const { name, description, price, stock } = req.body;
    let xquery;
    try {
      xquery = `INSERT INTO product (name, description, price, stock) VALUES ('${name}', '${description}', ${price}, ${stock})`;
      const Response = await DBservice.runQuery(xquery);

      Logger.info(["Product Helper", "SUCCESS Post Product"]);
      resolve(Response);
    } catch (err) {
      Logger.info(["Product Helper", "ERROR Post Product"], err.message);
      reject({ "err.message": err });
    }
  });
};

const updateProduct = (req) => {
  return new Promise(async (resolve, reject) => {
    const { name, description, price, stock } = req.body;
    const { id } = req.query;
    let xquery;
    try {
      xquery = `UPDATE product SET name = '${name}', description = '${description}', price = ${price}, stock = ${stock} WHERE id = ${id}`;
      const Response = await DBservice.runQuery(xquery);

      Logger.info(["Product Helper", "SUCCESS update Product"]);
      resolve(Response);
    } catch (err) {
      Logger.info(["Product Helper", "ERROR update Product"], err.message);
      reject({ "err.message": err });
    }
  });
};

const deleteProduct = (req) => {
  return new Promise(async (resolve, reject) => {
    const { id } = req.query;
    let xquery;
    try {
      xquery = `DELETE FROM product WHERE id = ${id}`;
      const Response = await DBservice.runQuery(xquery);

      Logger.info(["Product Helper", "SUCCESS delete Product"]);
      resolve(Response);
    } catch (err) {
      Logger.info(["Product Helper", "ERROR delete Product"], err.message);
      reject({ "err.message": err });
    }
  });
};

module.exports = {
  getProduct,
  postProduct,
  updateProduct,
  deleteProduct,
};
