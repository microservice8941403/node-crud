const router = require("express").Router();
const Product = require("../controller/ControllerProduct");
const Logger = require("../service/logger");
const validate = require("../service/validation");

const getProduct = async (req, res) => {
  let response;
  try {
    const getProduct = await Product.getProduct(req);
    Logger.info(["SUCCESS Get Product"]);
    response = {
      status: 200,
      message: "Success Get Product",
      data: getProduct,
    };
    if (!getProduct[0]) {
      response = {
        message: "Data Not Found",
      };
      return res.status(404).send(response);
    }
    return res.status(200).send(response);
  } catch (err) {
    const response = {
      status: "Internal Server Error",
      data: "The GET product has failed due to an internal server error.",
    };
    Logger.error(["FAILED Get Product"], err);
    return res.status(500).send(response);
  }
};

const postProduct = async (req, res) => {
  let response;
  try {
    const { error } = validate.validateProductPost(req.body);
    if (error) {
      response = {
        status: 400,
        message: error.details[0].message,
      };
      return res.status(400).send(response);
    }

    const getProduct = await Product.postProduct(req);
    Logger.info(["SUCCESS Post Product"]);
    response = {
      status: 200,
      message: "Success Add Product",
    };
    return res.status(200).send(response);
  } catch (err) {
    response = {
      status: "Internal Server Error",
      data: "The Post product has failed due to an internal server error.",
    };
    Logger.error(["FAILED Post Product"], err);
    return res.status(500).send(response);
  }
};

const updateProduct = async (req, res) => {
  let response;
  try {
    const { error } = validate.validateProductUpdate(req.query);
    if (error) {
      response = {
        status: 400,
        message: error.details[0].message,
      };
      return res.status(400).send(response);
    }

    const updateProduct = await Product.updateProduct(req);
    Logger.info(["SUCCESS update Product"]);
    response = {
      status: 200,
      message: "Success update Product",
    };
    return res.status(200).send(response);
  } catch (err) {
    response = {
      status: "Internal Server Error",
      data: "The Post product has failed due to an internal server error.",
    };
    Logger.error(["FAILED Post Product"], err);
    return res.status(500).send(response);
  }
};

const deleteProduct = async (req, res) => {
  let response;
  try {
    const { error } = validate.validateProductPost(req.body);
    if (error) {
      response = {
        status: 400,
        message: error.details[0].message,
      };
      return res.status(400).send(response);
    }

    const updateProduct = await Product.updateProduct(req);
    Logger.info(["SUCCESS delete Product"]);
    response = {
      status: 200,
      message: "Success delete Product",
    };
    return res.status(200).send(response);
  } catch (err) {
    response = {
      status: "Internal Server Error",
      data: "The Post product has failed due to an internal server error.",
    };
    Logger.error(["FAILED Post Product"], err);
    return res.status(500).send(response);
  }
};

router.get("/product", getProduct);
router.post("/product", postProduct);
router.put("/product", updateProduct);
router.delete("/product", deleteProduct);

module.exports = router;
